package main

import (
	"flag"
	"fmt"
	"io"
	"os"
)

func main() {
	proj, err := setupParseFlags(os.Stdout, os.Args)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	errs := validateFlags(proj)
	if len(errs) > 0 {
		for _, e := range errs {
			fmt.Println(e)
		}
		os.Exit(1)
	}
	generateScaffold(os.Stdout, *proj)
}

func setupParseFlags(w io.Writer, args []string) (*Project, error) {
	fs := flag.NewFlagSet("scaffold-gen", flag.ExitOnError)

	fs.SetOutput(w)

	location := fs.String("d", "", "Project location on disk")
	name := fs.String("n", "", "Project name")
	repository := fs.String("r", "", "Project remote repository URL")
	isStatic := fs.Bool("s", false, "Project will have static assets or not")
	proj := Project{}

	if len(args) > 1 {
		err := fs.Parse(args[1:])
		proj.Location = *location
		proj.Name = *name
		proj.Repository = *repository
		proj.IsStatic = *isStatic
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
	}
	return &proj, nil
}

func validateFlags(proj *Project) []error {
	var errs []error
	if proj.Location == "" {
		errs = append(errs, fmt.Errorf("Project path cannot be empty"))
	}
	if proj.Name == "" {
		errs = append(errs, fmt.Errorf("Project name cannot be empty"))
	}
	if proj.Repository == "" {
		errs = append(errs, fmt.Errorf("Project repository URL cannot be empty"))
	}
	return errs
}

func generateScaffold(w io.Writer, proj Project) {
	s := fmt.Sprintf("Generating scaffold for project %s in %s", proj.Name, proj.Location)
	w.Write([]byte(s))
}

type Project struct {
	Location   string
	Name       string
	Repository string
	IsStatic   bool
}
