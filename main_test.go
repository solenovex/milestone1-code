package main

import (
	"bytes"
	"fmt"
	"testing"
)

func TestSetupFlags(t *testing.T) {
	var tests = []struct {
		args     []string
		expected Project
	}{
		{
			args: []string{"go run .", "-d", "./project1", "-n", "Project1", "-r", "github.com/username/project1"},
			expected: Project{
				Location:   "./project1",
				Name:       "Project1",
				Repository: "github.com/username/project1",
				IsStatic:   false,
			},
		},
		{
			args: []string{"go run .", "-n", "Project1"},
			expected: Project{
				Location:   "",
				Name:       "Project1",
				Repository: "",
				IsStatic:   false,
			},
		},
	}

	b := new(bytes.Buffer)

	for _, test := range tests {
		proj, err := setupParseFlags(b, test.args)
		if err != nil {
			t.Error(err)
		} else {
			if proj.Name != test.expected.Name || proj.Location != test.expected.Location || proj.Repository != test.expected.Repository || proj.IsStatic != test.expected.IsStatic {
				t.Errorf("setupParseFlags(os.Stdout, %v), expected %v, got %v", test.args, test.expected, proj)
			}
		}
	}
}

func TestValidate(t *testing.T) {
	tests := []struct {
		proj     Project
		expected []error
	}{
		{
			Project{
				Location:   "./project1",
				Name:       "Project1",
				Repository: "github.com/username/project1",
				IsStatic:   false,
			},
			[]error{},
		},
		{
			Project{
				Location:   "",
				Name:       "Project1",
				Repository: "",
				IsStatic:   false,
			},
			[]error{
				fmt.Errorf("Project path cannot be empty"),
				fmt.Errorf("Project repository URL cannot be empty"),
			},
		},
	}

	for _, test := range tests {
		errs := validateFlags(&test.proj)
		success := true
		if len(errs) != len(test.expected) {
			success = false
		} else {
			for i := 0; i < len(errs); i++ {
				if errs[i].Error() != test.expected[i].Error() {
					success = false
				}
			}
		}
		if !success {
			t.Errorf("validateFlags(%v), expected: %v, got: %v", test.proj, test.expected, errs)
		}
	}
}

func TestGenerateScaffold(t *testing.T) {
	tests := []struct {
		proj     Project
		writer   *bytes.Buffer
		expected string
	}{
		{
			Project{
				Location:   "./project1",
				Name:       "Project1",
				Repository: "github.com/username/project1",
				IsStatic:   false,
			},
			new(bytes.Buffer),
			"Generating scaffold for project Project1 in ./project1",
		},
		{
			Project{
				Location:   "./project2",
				Name:       "Project2",
				Repository: "github.com/username/project2",
				IsStatic:   false,
			},
			new(bytes.Buffer),
			"Generating scaffold for project Project2 in ./project2",
		},
	}

	for _, test := range tests {
		generateScaffold(test.writer, test.proj)
		if test.writer.String() != test.expected {
			t.Errorf("generateScaffold(%v, %v), expected: %v, got: %v", test.writer, test.proj, test.expected, test.writer.String())
		}
	}
}
